gqrx-sdr (2.17.6-1) unstable; urgency=medium

  * New upstream release.
  New: Fetch RDS Program Service & RadioText via remote control.
  New: Set and query audio mute via remote control.
  Improved: Save I/Q recording format to settings.
  Improved: Reduced CPU utilization of plot and waterfall display.
  Improved: Display and formatting of RDS data.
  Fixed: Decoding of RDS flags.
  Fixed: Incorrect channel filter offset, for some devices.

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 30 Nov 2024 11:24:15 -0500

gqrx-sdr (2.17.5-1) unstable; urgency=medium

  * New upstream release.
    Fixed: Respond correctly to pipelined remote control commands.
    Fixed: Limit UDP packet size to 1024, for compatibility with netcat.

 -- A. Maitland Bottoms <bottoms@debian.org>  Thu, 18 Apr 2024 19:25:06 -0400

gqrx-sdr (2.17.4-2) unstable; urgency=medium

  * merge Janitor pull requests:
  * Use secure URI in Homepage field.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Set upstream metadata fields: Repository.
  * fix volk dependency

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 24 Feb 2024 21:59:29 -0500

gqrx-sdr (2.17.4-1) unstable; urgency=medium

  * New upstream release.
    Reply to \get_powerstat to fix wsjtx

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 10 Feb 2024 11:07:31 -0500

gqrx-sdr (2.17.3-1) unstable; urgency=medium

  * New upstream release.
    Fixed: Delete key shortcut for bookmark removal.

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 21 Oct 2023 12:21:10 -0400

gqrx-sdr (2.17.1-1) unstable; urgency=medium

  * New upstream release.

 -- A. Maitland Bottoms <bottoms@debian.org>  Mon, 09 Oct 2023 14:18:36 -0400

gqrx-sdr (2.17-2) unstable; urgency=medium

  * now building with Qt6
  * have debian/rules clean what CMake doesn't (Closes: #1044990)

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 03 Oct 2023 19:20:47 -0400

gqrx-sdr (2.17-1) unstable; urgency=medium

  * New upstream release.

 -- A. Maitland Bottoms <bottoms@debian.org>  Sun, 01 Oct 2023 17:00:28 -0400

gqrx-sdr (2.16-1) unstable; urgency=medium

  * New upstream release.
  * Adjust volk dependencies. (Closes: #1041529)

 -- A. Maitland Bottoms <bottoms@debian.org>  Sun, 30 Apr 2023 11:28:26 -0400

gqrx-sdr (2.15.9-1) unstable; urgency=medium

  * New upstream release.

 -- A. Maitland Bottoms <bottoms@debian.org>  Wed, 24 Aug 2022 20:17:08 -0400

gqrx-sdr (2.15.8-1) unstable; urgency=medium

  * New upstream release.
    fixed bug that caused a segfault when DSP paused for too long.

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 25 Jan 2022 20:43:51 -0500

gqrx-sdr (2.15.7-1) unstable; urgency=medium

  * New upstream release,
    Fix errors & warnings in appstream metadata & desktop entry

 -- A. Maitland Bottoms <bottoms@debian.org>  Sun, 23 Jan 2022 12:12:37 -0500

gqrx-sdr (2.15.6-1) unstable; urgency=medium

  * remove unnecessary dependencies

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 22 Jan 2022 14:54:30 -0500

gqrx-sdr (2.15.5-1) unstable; urgency=medium

  * New upstream release.
  * upstream updated gqrx.desktop (Closes: #1000331)

 -- A. Maitland Bottoms <bottoms@debian.org>  Thu, 20 Jan 2022 21:30:17 -0500

gqrx-sdr (2.15.4-1) unstable; urgency=medium

  * New upstream release.
  * updated Build-depends (Closes: #1003312)

 -- A. Maitland Bottoms <bottoms@debian.org>  Sun, 16 Jan 2022 15:21:59 -0500

gqrx-sdr (2.14.6-1) unstable; urgency=medium

  * New upstream release.
  * remove unnecessary pulseaudio dependency (Closes: #989625)

 -- A. Maitland Bottoms <bottoms@debian.org>  Fri, 10 Dec 2021 23:24:11 -0500

gqrx-sdr (2.14.4-1) unstable; urgency=medium

  * New upstream release

 -- A. Maitland Bottoms <bottoms@debian.org>  Mon, 28 Dec 2020 19:25:53 -0500

gqrx-sdr (2.14.3-1) unstable; urgency=medium

  * New upstream release

 -- A. Maitland Bottoms <bottoms@debian.org>  Wed, 09 Dec 2020 11:27:41 -0500

gqrx-sdr (2.14-1) unstable; urgency=medium

  * New upstream release

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 21 Nov 2020 19:30:55 -0500

gqrx-sdr (2.13.5-1) unstable; urgency=medium

  * New upstream release
  FIXED: Set correct buffer size for audio FFT.
  NEW: Added man page for use by distributions.
  FIXED: Crash when time span is set and waterfall height is set to 0%.
  FIXED: Filter width is incorrectly set when demod is off.
  IMPROVED: Removed Boost.Format dependency.

 -- A. Maitland Bottoms <bottoms@debian.org>  Sun, 08 Nov 2020 00:52:22 -0500

gqrx-sdr (2.13.3-1) unstable; urgency=medium

  * New upstream release
    FIXED: Crash when waterfall height is set to 100%.
    NEW: Preliminary support for GNU Radio 3.9.
    NEW: Print UDP configuration.
    NEW: Viridis color map.
    FIXED: Loss of precision in the plotter's frequency axis.
    FIXED: Usability on smaller screen sizes.
  * update Standards-version

 -- A. Maitland Bottoms <bottoms@debian.org>  Sun, 01 Nov 2020 14:34:57 -0500

gqrx-sdr (2.13.1-1) unstable; urgency=medium

  * New upstream release

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 17 Oct 2020 13:17:03 -0400

gqrx-sdr (2.13-1) unstable; urgency=medium

  * New upstream release
  * update to v2.13-2-g19772b1

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 17 Oct 2020 13:16:36 -0400

gqrx-sdr (2.12.1-1) unstable; urgency=medium

  * New upstream release
  * update to v2.12.1-8-g8c1d8d4

 -- A. Maitland Bottoms <bottoms@debian.org>  Sun, 02 Feb 2020 21:13:01 -0500

gqrx-sdr (2.11.5-2) unstable; urgency=medium

  * update to argilo/maint-3.8 v2.11.5-13-ga02d54c
  * Build using gnuradio 3.8

 -- A. Maitland Bottoms <bottoms@debian.org>  Fri, 06 Sep 2019 00:05:34 -0400

gqrx-sdr (2.11.5-1) unstable; urgency=medium

  * New upstream bugfix release
    fixes a crash in the handling of the ‘q’ command in the
    remote control interface

 -- A. Maitland Bottoms <bottoms@debian.org>  Mon, 21 May 2018 00:13:40 -0400

gqrx-sdr (2.11.4-1) unstable; urgency=medium

  * New upstream release

 -- A. Maitland Bottoms <bottoms@debian.org>  Sun, 13 May 2018 19:58:58 -0400

gqrx-sdr (2.9-2) unstable; urgency=medium

  * upstream Debian patches, update to v2.9-4-g4138038

 -- A. Maitland Bottoms <bottoms@debian.org>  Sun, 10 Dec 2017 16:53:16 -0500

gqrx-sdr (2.9-1) unstable; urgency=medium

  * New upstream release

 -- A. Maitland Bottoms <bottoms@debian.org>  Wed, 15 Nov 2017 23:06:55 -0500

gqrx-sdr (2.8-1) unstable; urgency=medium

  * New upstream release
   New features
     Remote control command to set LNB LO (LNB_LO).
   Bugs fixed
     Inactive Start DSP and Config buttons in toolbar.
     Frequency controller digit not redrawn after a resize.
     Set default mode to AM instead of none.
   Miscellaneous improvements
     Detection of gr-osmosdr when installed in custom directory.
     Remote frequency handling.

 -- A. Maitland Bottoms <bottoms@debian.org>  Fri, 22 Sep 2017 20:22:01 -0400

gqrx-sdr (2.7-1) unstable; urgency=medium

  * New upstream release

 -- A. Maitland Bottoms <bottoms@debian.org>  Mon, 11 Sep 2017 10:10:14 -0400

gqrx-sdr (2.6-1) unstable; urgency=medium

  * New upstream release
  * update to v2.6-7-g82dafcc
    NEW: Save remote control state between sessions.
    FIXED: Keep waterfall zoom level and zoom slider synchronised.

 -- A. Maitland Bottoms <bottoms@debian.org>  Fri, 14 Oct 2016 21:09:33 -0400

gqrx-sdr (2.5.3-4) unstable; urgency=medium

  * install icon usr/share/icons/hicolor/scalable/apps/gqrx.svg
  * Add SoapySDR support

 -- A. Maitland Bottoms <bottoms@debian.org>  Sun, 28 Aug 2016 14:20:14 -0400

gqrx-sdr (2.5.3-3) unstable; urgency=medium

  * update to v2.5.3-90-g3556cbf
  * buuld with current boost (Closes: #822133, #823152)

 -- A. Maitland Bottoms <bottoms@debian.org>  Sun, 28 Aug 2016 11:19:46 -0400

gqrx-sdr (2.5.3-2) unstable; urgency=medium

  * update to v2.5.3-51-gbf2f5f8

 -- A. Maitland Bottoms <bottoms@debian.org>  Mon, 11 Jul 2016 22:45:37 -0400

gqrx-sdr (2.5.3-1) unstable; urgency=medium

  * New upstream release
  * update to v2.5.3-25-gba612ea

 -- A. Maitland Bottoms <bottoms@debian.org>  Fri, 29 Apr 2016 21:58:38 -0400

gqrx-sdr (2.5.1-1) unstable; urgency=medium

  * New upstream release

 -- A. Maitland Bottoms <bottoms@debian.org>  Thu, 14 Jan 2016 22:39:30 -0500

gqrx-sdr (2.4-1) unstable; urgency=medium

  * New upstream release

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 12 Dec 2015 17:09:48 -0500

gqrx-sdr (2.3.2.193-2) unstable; urgency=medium

  * Lintian fixes - reflect upstream layout changes in copyright file

 -- A. Maitland Bottoms <bottoms@debian.org>  Wed, 02 Dec 2015 00:43:02 -0500

gqrx-sdr (2.3.2.193-1) unstable; urgency=medium

  * New upstream - update to v2.3.2-193-gbbed2c9
    cmake build

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 01 Dec 2015 18:46:54 -0500

gqrx-sdr (2.3.2.121-1) unstable; urgency=medium

  * New upstream - update to v2.3.2-121-g3a51618 (Closes: #773008)
  * add watch file
  * update URL in man page (Closes: #794917)
  * update dependencies (Closes:  #795483, #765413, #763832)
  * install desktop file (Closes: #776092)

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 15 Sep 2015 20:04:21 -0400

gqrx-sdr (2.3.1-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Update dependency for libvolk to new package name:
   - libvolk0.0.0 -> libvolk1.1

 -- Iain R. Learmonth <irl@debian.org>  Thu, 03 Sep 2015 23:13:35 +0100

gqrx-sdr (2.3.1-2) unstable; urgency=low

  * Build with libpulse-dev (Closes: #755940)

 -- A. Maitland Bottoms <bottoms@debian.org>  Fri, 12 Sep 2014 22:33:32 -0400

gqrx-sdr (2.3.1-1) unstable; urgency=low

  * New upstream release (Closes: #760332)
  * Use libgnuradio-osmosdr0.1.3 (Closes: #749749)

 -- A. Maitland Bottoms <bottoms@debian.org>  Fri, 05 Sep 2014 00:22:52 -0400

gqrx-sdr (2.2.0.137.eca0ea7-1) unstable; urgency=low

  * Update to eca0ea7
  * Rebuild to use libgnuradio-osmosdr0.1.1 (Closes: #747943)

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 13 May 2014 23:52:09 -0400

gqrx-sdr (2.2.0.74.d97bd7-2~bpo70~1) wheezy-backports; urgency=low

  * Rebuild for wheezy-backports.

 -- A. Maitland Bottoms <bottoms@debian.org>  Mon, 03 Mar 2014 14:11:48 -0500

gqrx-sdr (2.2.0.74.d97bd7-2) unstable; urgency=low

  * Update Build-depends

 -- A. Maitland Bottoms <bottoms@debian.org>  Wed, 01 Jan 2014 20:52:10 -0500

gqrx-sdr (2.2.0.74.d97bd7-1) unstable; urgency=low

  * New upstream commit, build with gnuradio 3.7.2.1
  * Tighten libvolk dependency version (Closes: #732043)
  * Built without portaudio.

 -- A. Maitland Bottoms <bottoms@debian.org>  Wed, 01 Jan 2014 17:38:31 -0500

gqrx-sdr (2.2.0.25.5d6be8-1) unstable; urgency=low

  * New package (Closes: #662073)
  * Match GNU Radio live distribution 2013-0926

 -- A. Maitland Bottoms <bottoms@debian.org>  Thu, 03 Oct 2013 19:22:58 -0400

gqrx-sdr (2.1~git29d6c18-1) unstable; urgency=low

  * New upstream git

 -- A. Maitland Bottoms <bottoms@debian.org>  Wed, 01 May 2013 17:57:52 -0400

gqrx-sdr (2.0-1) unstable; urgency=low

  * New upstream release

 -- A. Maitland Bottoms <bottoms@debian.org>  Mon, 28 May 2012 17:38:22 -0400

gqrx-sdr (1.9.0.abd2dc8-2) unstable; urgency=low

  * correct build dependencies using a clean chroot.

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 15 May 2012 00:18:27 -0400

gqrx-sdr (1.9.0.abd2dc8-1) unstable; urgency=low

  * Package from new upstream pre-release (Closes: #662073)

 -- A. Maitland Bottoms <bottoms@debian.org>  Mon, 14 May 2012 22:32:40 -0500
